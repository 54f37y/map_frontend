map_frontend
===

A Vue project to display a map of road safety features and
hazards. Hosted at [map.safe7y.com](https://map.safe7y.com).


## Developement setup

The following instructions setup the front-end for 
development work on Ubuntu 20.04. Milage may vary.

- Clone the repo `git clone git@gitlab.com:54f37y/map_frontend.git`
- Install Nodejs `curl -fsSL https://deb.nodesource.com/setup_14.x | sudo -E bash -`
`sudo apt-get install -y nodejs` 
- Install yarn using the Nodejs package manager `npm` `sudo npm install --global yarn`
- Navigate to the directory `cd map_frontend`
- Install the appropriate packages `npm install`
- Run the dev server `yarn run serve`
- Head over to [http://localhost:8030/#prod](http://localhost:8030/#prod) to see the result
	+ We add the `#prod` to allow the frontend to run without 
	map_backend

## Deployment

During deployment the mimified js code is deployed to the server via rsync. In order to do 
this you'll need SSH access to the server you wish to deploy to and you'll need to have
that server setup in your `.ssh/config` file. The script expects the deployment server
to be named `safety-dev`.


The following instructions work on Ubuntu 20.04. Milage may vary. They also assume you 
have completed the Development Setup above.

- Navigate to the top level directory `cd map_frontend`
- Lint the project and fix files `yarn lint` 
- Build the project `yarn run build || npm run build`
- Copy the project to the server `rsync -azP dist/* safety-dev:site/public/`


## Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
