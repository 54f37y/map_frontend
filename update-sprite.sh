MBSPRITE=node_modules/mbsprite/mbsprite
LIGHTSPRITE='https://api.mapbox.com/styles/v1/mapbox/light-v9/sprite?access_token=pk.eyJ1IjoiYWlkZW5yb2hkZSIsImEiOiJja2tzZ2JnY3cwNnJ0MnZtcGdhNXhqanZjIn0.JJ1CZxjQKFC1Hfidt3KNgg'
MAPILLARYSPRITE='https://www.mapillary.com/main/assets/sprites/v1/sprites'
mkdir -p tmp/src
mkdir -p public/sprite
${MBSPRITE} explode "${LIGHTSPRITE}" tmp/src
${MBSPRITE} explode "${MAPILLARYSPRITE}" tmp/src
cp icons@1x/* tmp/src
cp icons@2x/* tmp/src@2x
${MBSPRITE} bundle tmp/out tmp/src tmp/src@2x
cp tmp/out/* public/sprite