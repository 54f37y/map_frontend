import Vue from 'vue';
const EventBus = new Vue();
EventBus.$$on = function(handlers) {
    for (const id of Object.keys(handlers)) {
        EventBus.$on(id, handlers[id]);
    }
};
export { EventBus };
