import * as d3 from 'd3-fetch';
async function deviceList() {
    return await d3.csv('210114-safe7y_features.csv');
}

export { deviceList };
