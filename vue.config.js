module.exports = {
    publicPath: '',
    devServer: {
        historyApiFallback: false,
        port: 8030,
    },
    transpileDependencies: ['vuetify'],
};
